from app.models import Address, db
from app.services.address_services import serialize_address_list, serialize_address
from flask import Blueprint, request
from http import HTTPStatus
from sqlalchemy.exc import IntegrityError
from app.services.http import build_api_response

bp_adresses = Blueprint('api_adresses', __name__, url_prefix='/adresses')


@bp_adresses.route('/')
def list_all():
    adresses = Address.query.all()
    address_dicts = serialize_address_list(adresses)

    return {'data': address_dicts}, HTTPStatus.OK


@bp_adresses.route('/<int:address_id>')
def get(address_id: int):
    address = Address.query.get_or_404(address_id)
    return {'data': serialize_address(address)}


@bp_adresses.route('/', methods=['POST'])
def create():
    data = request.get_json()
    address = Address(
        street=data.get('street'),
        number=data.get('number'),
        addr_line_1=data.get('addr_line_1'),
        addr_line_2=data.get('addr_line_2'),
        postal_code=data.get('postal_code')
    )

    try:
        db.session.add(address)
        db.session.commit()
        return build_api_response(HTTPStatus.CREATED)
    except IntegrityError:
        return build_api_response(HTTPStatus.BAD_REQUEST)
