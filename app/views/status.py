from flask import Blueprint

bp_status = Blueprint('api_status', __name__, url_prefix='/status')


@bp_status.route('/')
def status():
    return {'msg': 'ok'}
