from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

db = SQLAlchemy()
mg = Migrate()

user_address = db.Table(
    'user_address',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('address_id', db.Integer, db.ForeignKey('address.id'))
)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=False, nullable=False)
    surname = db.Column(db.String, unique=False, nullable=True)
    document = db.Column(db.String(1024), unique=True, nullable=False)

    accounts = db.relationship("CCAccount", back_populates="user")
    adresses = db.relationship("Address", secondary=user_address, back_populates='users')

    def __repr__(self):
        return f'<User {self.name} {self.document}>'


class CCAccount(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    agency = db.Column(db.String, nullable=False)
    number = db.Column(db.String, nullable=False)
    money = db.Column(db.Float, nullable=False)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    user = db.relationship("User", back_populates="accounts")


class Address(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    street = db.Column(db.String, nullable=False)
    number = db.Column(db.Integer, nullable=True)
    addr_line_1 = db.Column(db.String, nullable=True)
    addr_line_2 = db.Column(db.String, nullable=True)
    postal_code = db.Column(db.String, nullable=False)

    users = db.relationship("User", secondary=user_address, back_populates="adresses")
